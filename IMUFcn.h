#ifndef IMUFcn

#include <stdio.h>
#include <math.h>
#include <unistd.h> 
#include <stdlib.h>
#include <fcntl.h> 
#include <termios.h> 
#include <errno.h>
#include <sys/ioctl.h>
#include <string.h>

#define ROLL 1
#define PITCH 2
#define YAW 3

#define EulerAngle 53
#define GyroRate 52

unsigned char ReadDataCommand[13];

float bytes2float(unsigned char* bytes_array);
char Checksum(char *msg, int len);
int SetDevice(char* device);
float GetValue(int fd, int input, int com);
void GetCommand(int cnt, int com);


#endif
