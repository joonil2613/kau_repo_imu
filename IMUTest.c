#include "IMUFcn.h"

int main(int argc, char *argv[])
{
	int count;
	int Loopnumber = 0;
	float Roll, Pitch, Yaw;
	float Roll_rate, Pitch_rate, Yaw_rate;
	
	int fd = SetDevice("/dev/ttyUSB0");
	printf("How many get IMU data?\r\n");
	scanf("%d", &count);
	printf("-------------------------------------\r\n\n");
	printf("Roll\t Pitch\t Yaw");
	printf("\t Roll_rate\t Pitch_rate\t Yaw_rate \r\n\n");
	printf("-------------------------------------\r\n\n");
	while(Loopnumber <= count)
	{
		Roll = GetValue(fd, ROLL, EulerAngle);
		Pitch = GetValue(fd, PITCH, EulerAngle);
		Yaw = GetValue(fd, YAW, EulerAngle);
		Roll_rate = GetValue(fd, ROLL, GyroRate);
		Pitch_rate = GetValue(fd, PITCH, GyroRate);
		Yaw_rate = GetValue(fd, YAW, GyroRate);
		printf("%0.2f\t %0.2f\t %0.2f", Roll, Pitch, Yaw);
		printf("\t %0.2f\t %0.2f\t %0.2f \r\n", Roll_rate, Pitch_rate, Yaw_rate);
		Loopnumber++;
	}
	close(fd);
	return 0;
}

