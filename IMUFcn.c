#include "IMUFcn.h"

unsigned char ReadDataCommand[13] = {0x02, 0x0D, 0x01, 0x3C, 0x00,
									0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
									0x00, 0x03};

float bytes2float(unsigned char* bytes_array)
{
	
  union {
    float float_variable;
    char temp[4];
  } u;
  
  for (int k = 0; k < 4; k++)
  {
	  u.temp[k] = bytes_array[k];
  }
  
  return u.float_variable;
}

char Checksum(char *msg, int len)
{
	char cs = 0;
	for (int j = 0; j < len; j++)
	{
		cs += msg[j + 3];
	}
	return cs;
}

void GetCommand(int cnt, int com)
{
	if (cnt == ROLL)
	{
		ReadDataCommand[6] = ROLL;
	}
	else if (cnt == PITCH)
	{
		ReadDataCommand[6] = PITCH;
	}
	else if (cnt == YAW)
	{
		ReadDataCommand[6] = YAW;
	}
	ReadDataCommand[4] = com;
	ReadDataCommand[11] = Checksum(ReadDataCommand, 8);
}

float GetValue(int fd, int input, int com)
{
 	unsigned char buf[13];
	int i;
	unsigned char buf2float[4];
	float Value = 0;

	GetCommand(input, com);
	
	int count = write(fd, ReadDataCommand, 13);
	count = read(fd, buf, 13);
	
	for (i = 0; i < 4; i++)
	{
		buf2float[i] = buf[i + 7];
	}
	
	Value = bytes2float(buf2float);
	return Value;
}


int SetDevice(char *device)
{
	int sfd = open(device, O_RDWR | O_NOCTTY ); 
	if (sfd == -1){
	  printf ("Error no is : %d\n", errno);
	  printf("Error description is : %s\n",strerror(errno));
	  return(-1);
	}
	struct termios options;
	tcgetattr(sfd, &options);
	options.c_cflag = B115200 | CS8 | CLOCAL | CREAD;		//<Set baud rate
//	options.c_cflag &= ~ PARENB;
//	options.c_cflag &= ~CSTOPB;
//	options.c_cflag &= ~CSIZE;
//	options.c_cflag &= ~CRTSCTS;
	cfsetispeed(&options,B115200);
	cfsetospeed(&options,B115200);
	options.c_iflag = IGNPAR;
//	options.c_iflag &= ~(IXON|IXOFF|IXANY);
//	options.c_iflag &= ~(ICANON|ECHO|ECHOE|ISIG);
	options.c_oflag = 0;
	options.c_lflag = 0;//ICANON;
//	options.c_cc[VMIN] = 1;
//	options.c_cc[VTIME] = 1;	
	tcflush(sfd, TCIFLUSH);
	if(tcsetattr(sfd, TCSANOW, &options)!=0)
		printf("Error\r\n");
	//set_blocking(fd,0)
	return sfd;
}
